import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {GetLinkService, Song} from '../get-link.service';
import {Subscription} from 'rxjs';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-frame',
  templateUrl: './frame.component.html',
  styleUrls: ['./frame.component.css']
})
export class FrameComponent implements OnInit, OnDestroy {
  song: Song;
  sub: Subscription;
  src = '';
  constructor(private route: ActivatedRoute, private getLinkService: GetLinkService, private domSanitizer: DomSanitizer) { }
  getSrc() {
    this.src = 'https://www.youtube.com/embed/' + this.song.id;
    return this.domSanitizer.bypassSecurityTrustResourceUrl(this.src);
  }
  ngOnInit() {
    this.sub = this.route.paramMap.subscribe((paramMap: ParamMap) => {
      const id = paramMap.get('id');
      this.song = this.getLinkService.find(id);
    });
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

}
