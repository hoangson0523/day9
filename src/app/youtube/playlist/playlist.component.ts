import { Component, OnInit } from '@angular/core';
import {GetLinkService} from '../get-link.service';

@Component({
  selector: 'app-playlist',
  templateUrl: './playlist.component.html',
  styleUrls: ['./playlist.component.css']
})
export class PlaylistComponent implements OnInit {

  constructor(private getLinkService: GetLinkService) { }

  ngOnInit() {
  }

}
