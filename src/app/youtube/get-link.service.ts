import { Injectable } from '@angular/core';
export interface Song {
  id: string;
  name: string;
}
@Injectable({
  providedIn: 'root'
})
export class GetLinkService {
  playlist = [
    {id: 'WOEMkG3gOKI', name: 'Perfect - Endless Summer'},
    {id: '2Vv-BfVoq4g', name: 'Perfect - Ed Sheeran'},
    {id: 'mWRsgZuwf_8', name: 'Demons - Imagine Dragons'},
    {id: '2GRP1rkE4O0', name: 'Blue - BigBang'},
    {id: 'dmVtts8oPjA', name: 'Nothing gonna change my love for you - Westlife'}
  ];
  constructor() { }

  find(id: string) {
    return this.playlist.find(item =>  item.id === id );
  }
}
