import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ImgSliderComponent } from './img-slider/img-slider.component';
import { ImgSlideComponent } from './img-slider/img-slide/img-slide.component';
import {RouterModule} from '@angular/router';
import { DictionaryComponent } from './dictionary/dictionary.component';
import { DictionarySearchComponent } from './dictionary/dictionary-search/dictionary-search.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { DictionaryDetailComponent } from './dictionary/dictionary-detail/dictionary-detail.component';
import { TimelineComponent } from './timeline/timeline.component';
import { YoutubeComponent } from './youtube/youtube.component';
import { PlaylistComponent } from './youtube/playlist/playlist.component';
import { FrameComponent } from './youtube/frame/frame.component';
import { GalleryComponent } from './gallery/gallery.component';
import { CardComponent } from './gallery/card/card.component';
import {GalleryConfig} from './gallery/token';

@NgModule({
  declarations: [
    AppComponent,
    ImgSliderComponent,
    ImgSlideComponent,
    DictionaryComponent,
    DictionarySearchComponent,
    DictionaryDetailComponent,
    TimelineComponent,
    YoutubeComponent,
    PlaylistComponent,
    FrameComponent,
    GalleryComponent,
    CardComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      {
        path: 'dictionary',
        component: DictionaryComponent,
        children: [
          {path: ':word', component: DictionaryDetailComponent}
        ]
      },
      {path: 'image-slide', component: ImgSliderComponent},
      {path: 'gallery', component: GalleryComponent},
      {path: 'youtube',
        component: YoutubeComponent,
      children: [
        {path: ':id', component: FrameComponent}
      ]},
      {path: 'live-counter', component: TimelineComponent}
    ]),
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [
    {provide: GalleryConfig, useValue: 2}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
