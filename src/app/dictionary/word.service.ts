import { Injectable } from '@angular/core';

export  interface Word {
  key: string;
  meaning: string;
}
@Injectable({
  providedIn: 'root'
})
export class WordService {
  private words: Word[] = [
    {
      key: 'apple',
      meaning: 'quả táo'
    }, {
      key: 'banana',
      meaning: 'quả chuối'
    }, {
      key: 'papaya',
      meaning: 'quả đu đủ'
    }, {
      key: 'dragon fruit',
      meaning: 'quả thanh long'
    }, {
      key: 'peaches',
      meaning: 'quả đào'
    }
  ];
  constructor() { }

  search(word: string) {
    if (!word) {
      return '';
    }
    const input = this.words.find(item => item.key === word.toLowerCase());
    if (input) {
      return input.meaning;
    } else {
      return 'Not found';
    }
  }
  getAll() {
    return this.words;
  }
}
