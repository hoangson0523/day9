import { Component, OnInit } from '@angular/core';
import {Word, WordService} from '../word.service';
import {FormControl, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-dictionary-search',
  templateUrl: './dictionary-search.component.html',
  styleUrls: ['./dictionary-search.component.css']
})
export class DictionarySearchComponent implements OnInit {
  dictionaryForm: FormGroup;
  result = '';
  constructor(private wordService: WordService) { }

  search() {
    const word = this.dictionaryForm.get('word').value;
    this.result = this.wordService.search(word);
  }
  ngOnInit() {
    this.dictionaryForm = new FormGroup({
      word: new FormControl()
    });
  }

}
