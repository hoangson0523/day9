import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {Word, WordService} from '../word.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-dictionary-detail',
  templateUrl: './dictionary-detail.component.html',
  styleUrls: ['./dictionary-detail.component.css']
})
export class DictionaryDetailComponent implements OnInit, OnDestroy {
  item: Word;
  sub: Subscription;
  constructor(private route: ActivatedRoute, private wordService: WordService) { }

  ngOnInit() {
    this.sub = this.route.paramMap.subscribe((paramMap: ParamMap) => {
      const wordKey = paramMap.get('word');
      this.item = {
        key: wordKey,
        meaning: this.wordService.search(wordKey)
      };
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
