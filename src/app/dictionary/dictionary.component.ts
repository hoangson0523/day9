import { Component, OnInit } from '@angular/core';
import {Word, WordService} from './word.service';

@Component({
  selector: 'app-dictionary',
  templateUrl: './dictionary.component.html',
  styleUrls: ['./dictionary.component.css']
})
export class DictionaryComponent implements OnInit {
  listWord: Word[];
  constructor(private wordService: WordService) { }

  ngOnInit() {
    this.listWord = this.wordService.getAll();
  }

}
