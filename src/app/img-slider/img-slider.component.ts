import {Component, OnInit} from '@angular/core';
import {ImageService} from './image.service';

@Component({
  selector: 'app-img-slider',
  templateUrl: './img-slider.component.html',
  styleUrls: ['./img-slider.component.css']
})
export class ImgSliderComponent implements OnInit {
  index = 0;
  src = '';
  constructor(private imageService: ImageService) { }

  ngOnInit() {
    this.src = this.imageService.images[this.index];
  }


  previous() {
    if (this.index === 0) {
      this.index = 3;
    }
    this.index--;
    this.src = this.imageService.images[this.index];
  }

  next() {
    this.index++;
    if (this.index === 3) {
      this.index = 0;
    }
    this.src = this.imageService.images[this.index];
  }
}
