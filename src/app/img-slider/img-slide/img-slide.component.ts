import {Component, Input, OnInit} from '@angular/core';
import {ImageService} from '../../gallery/image.service';

@Component({
  selector: 'app-img-slide',
  templateUrl: './img-slide.component.html',
  styleUrls: ['./img-slide.component.css']
})
export class ImgSlideComponent implements OnInit {
  @Input()
  imageSrc;
  constructor() { }

  ngOnInit() {
  }

}
