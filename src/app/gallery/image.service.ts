import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImageService {
  images = [
    'https://news.nationalgeographic.com/content/dam/news/2018/05/17/you-can-train-your-cat/02-cat-training-NationalGeographic_1484324.jpg',
    'https://a57.foxnews.com/static.foxnews.com/foxnews.com/content/uploads/2019/07/931/524/creepy-cat.jpg?ve=1&tl=1',
    'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQvmdG435HxyF0e1DP1IBVos10zTwuNJ0p9M_iYDzlYWup6AgfV6w',
    'https://i1.wp.com/www.usmagazine.com/wp-content/uploads/2018/06/' +
    'Smoothie-the-Cat-Instagram-zoom.jpg?crop=0px%2C0px%2C1080px%2C611px&resize=1200%2C675&ssl=1'
  ];
  constructor() { }
}
