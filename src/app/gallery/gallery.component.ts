import {Component, Inject, OnInit, Optional} from '@angular/core';
import {ImageService} from './image.service';
import {GalleryConfig} from './token';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {
  itemWidth: number;
  config = 4;
  constructor(private imageService: ImageService, @Inject(GalleryConfig) @Optional() config: number) {
    if (config) {
      this.config = config;
    }
  }

  ngOnInit() {
    this.itemWidth = (100 / this.config);
  }

}
