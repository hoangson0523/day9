import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {DateServiceService} from './date-service.service';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.css']
})
export class TimelineComponent implements OnInit {
  timelineForm: FormGroup;
  output = '';
  constructor(private dateService: DateServiceService) { }

  calculate() {
    this.output = this.dateService.getDifferenceToNow(this.timelineForm.get('dob').value);
  }
  ngOnInit() {
    this.timelineForm = new FormGroup({
      dob: new FormControl()
    });
  }

}
