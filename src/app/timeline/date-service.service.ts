import { Injectable } from '@angular/core';
import { addMonths, addYears, differenceInDays, differenceInMonths, differenceInYears } from 'date-fns';
@Injectable({
  providedIn: 'root'
})
export class DateServiceService {

  constructor() { }

  getDifferenceToNow(diff: string | number | Date): string {
    let result = '';
    const now = new Date();
    diff = new Date(diff);
    const years = differenceInYears(now, diff);
    if (years > 0) {
      result += `${years} years `;
      diff = addYears(diff, years);
    }
    const months = differenceInMonths(now, diff);
    if (years > 0) {
      result += `${months} months `;
      diff = addMonths(diff, months);
    }
    const days = differenceInDays(now, diff);
    if (days > 0) {
      result += `${days} days`;
    }
    return result;
  }
}
